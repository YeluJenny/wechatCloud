package com.tencent.wxcloudrun.controller;

import com.tencent.wxcloudrun.aes.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * index控制器
 */
@Controller

public class IndexController {
  private Logger logger = LoggerFactory.getLogger(IndexController.class);
  private static final String TOKEN = "test20221013";

  /**
   * 主页页面
   * @return API response html
   */
  @GetMapping
  public String index() {
    return "index";
  }

  /**
   * 用于校验服务器是否合规
   */
  @RequestMapping(value = "/checkToken")
  public String wxSignatureCheck(String signature, String timestamp, String nonce, String echostr){

    //1、排序
    String[] arr = {TOKEN, timestamp, nonce};
    Arrays.sort(arr);

    //2、拼接
    StringBuilder content = new StringBuilder();
    for (int i = 0; i < arr.length; i++) {
      content.append(arr[i]);
    }

    //sha1Hex 加密
    MessageDigest md = null;
    String temp =  sha1(content.toString());
    logger.info("加密后的token:"+temp);
    logger.info("传入的token:"+signature);
    if (!temp.equals("") && temp.equals(signature)){
      logger.info("匹配成功");
      return echostr;
    }
    return null;
  }

  private static String sha1(String temp){
    try {
      MessageDigest digest = MessageDigest
              .getInstance("SHA-1");
      digest.update(temp.getBytes());
      byte messageDigest[] = digest.digest();
      // Create Hex String
      StringBuffer hexString = new StringBuffer();
      // 字节数组转换为 十六进制 数
      for (int i = 0; i < messageDigest.length; i++) {
        String shaHex = Integer.toHexString(messageDigest[i] & 0xFF);
        if (shaHex.length() < 2) {
          hexString.append(0);
        }
        hexString.append(shaHex);
      }
      return hexString.toString();

    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return "";
  }
}
